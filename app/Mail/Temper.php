<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Temper extends Mailable
{
    use Queueable, SerializesModels;

    public $temp;

  /**
   * Создание нового экземпляра сообщения.
   *
   * @return void
   */
  public function __construct(String $temp)
  {
    $this->temp = $temp;
  }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //нужна настройка в env
        return $this->view('wether');
          // prepare email body text


    }
}
