<?php

namespace App\Http\Controllers;

use App\Test;
use App\Autors;
use App\Books;
use App\ab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
    
        public function getAutors(Request $request)
        {

            $au = \App\Autors::all();

            return $au->toJson();
        }
        
        public function getBooks(Request $request)
        {

            $b = \App\Books::all();

            return $b->toJson();
        }
        
       public function getAuofB(Request $request)
        {

            $AuofB = \App\ab::where('idb', $request->idb)->get();
            $AuofB = DB::table('abs')
            ->join('autors', 'abs.ida', '=', 'autors.id')
            ->select('abs.*', 'autors.*')
            ->where('idb', $request->idb)
            ->get();

            return $AuofB->toJson();
        }
        
        public function getBofAu(Request $request)
        {

            $AuofB = \App\ab::where('ida', $request->ida)->get();
            $AuofB = DB::table('abs')
            ->join('books', 'abs.idb', '=', 'books.id')
            ->select('abs.*', 'books.*')
            ->where('ida', $request->ida)
            ->sum('price');

            return $AuofB;
        }
        
        public function getBWithoutAu(Request $request)
        {

            $AuofB = DB::table('books')
            ->leftjoin('abs', 'abs.idb', '=', 'books.id')
            ->select('abs.*', 'books.*')
            ->whereNull('ida')
            ->get();

             return $AuofB->toJson();
        }
}
