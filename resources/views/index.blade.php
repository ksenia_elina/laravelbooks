    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>Laravel</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <!-- Fonts -->
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
            <link rel="stylesheet" href="{{ URL::asset('public/css/css.css') }}">
        </head>
         <script>
            $.ajax({
                type: 'get',
                url:'/getBooks',
                //data: {
                //    project_id: projectID
                //},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                success: function(data) {

                    var options = '';
                    var s =  document.querySelector('.dropdown').options;

                    $.each(JSON.parse(data), function(i, item) {

                        s[s.length]= new Option(item.name,item.id, true);

                       
                           //alert(item.id);

                    });

                }
            });
            
            
            $.ajax({
                type: 'get',
                url:'/getAutors',
                //data: {
                //    project_id: projectID
                //},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                success: function(data) {

                    var options = '';
                    var s =  document.querySelector('.dropdownAu').options;
                    $.each(JSON.parse(data), function(i, item) {
                        s[s.length]= new Option(item.name,item.id, true);


                    });

                    $(document).ready(function(){
                   // alert(jQuery.fn.jquery);
                    });
                }
            });
            
               function changeFunc() {
        var selectBox = document.getElementById("dropdown");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
         $.ajax({
                type: 'post',
                url:'/getAuofB',
                data: {
                    "_token": "{{ csrf_token() }}",
                    idb: selectBox.options[selectBox.selectedIndex].value
                },
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                success: function(data) {

                    $.each(JSON.parse(data), function(i, item) {
                        alert(item.name);

                    });

                }
            });
       }
       
             function changeFuncA() {
        var selectBox = document.getElementById("dropdownAu");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        alert(selectedValue);
         $.ajax({
                type: 'post',
                url:'/getBofAu',
                data: {
                    "_token": "{{ csrf_token() }}",
                    ida: selectBox.options[selectBox.selectedIndex].value
                },
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                success: function(data) {
                    alert(data);


                }
            });
       }
       
                function WithoutA() {
        var selectBox = document.getElementById("dropdownAu");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
         $.ajax({
                type: 'get',
                url:'/getBWithoutAu',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                success: function(data) {
                    $.each(JSON.parse(data), function(i, item) {
                        alert(item.name);

                    });


                }
            });
       }

        
    </script>
        <body>
            <select class="dropdown" id="dropdown" onchange="changeFunc();"></select>
            <select class="dropdownAu" id="dropdownAu" onchange="changeFuncA();"></select>
            <button onclick="WithoutA();">Книги без авторов</button>
            </div>
        </body>
       
    </html>

